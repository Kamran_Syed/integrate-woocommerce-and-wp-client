<?php
/*
Plugin Name: Aspk Wp Client
Plugin URI: 
Description: Aspk Wp Client Upload Shares and Link
Version: 1.2
Author: AgileSolutionspk
Author URI: http://agilesolutionspk.com/
*/
//require_once('aweber_api/aweber_api.php');
if ( !class_exists( 'aspk_wp_client' )){   
	class aspk_wp_client{
		
		function aspk_operation($order_id){
			global $wpdb;
			
			$meta_values 	= get_post_meta( $order_id );
			$user_id 		= $meta_values['_customer_user']['0'];
			$user = get_user_by( 'id', $user_id );
			
			
			$user_info = get_userdata($user_id);
			if($user_info){
				$user_email = $user_info->user_email;
				//$this->aweber_add_subscriber($user_email);
			}
			$user_role = get_user_meta($user_id, $wpdb->prefix.'capabilities', true);
			if($user_role){
				$role = array_keys($user_role);
				if($user_id != 0 && $role[0] == 'customer'){
					$new_user_role = array();
					$new_user_role['wpc_client'] = 1;
					update_user_meta($user_id, $wpdb->prefix.'capabilities', $new_user_role,$user_role);
					$post_hub_pgid = $this->create_hubpage($user->data->user_nicename);
					$post_clients_pgid = $this->create_clientspage($user->data->user_nicename);
					update_user_meta($user_id, 'wpc_cl_hubpage_id', $post_hub_pgid);
					$this->assign_clients_portal_page($post_clients_pgid,$user_id);
				}elseif($user_id != 0){
					
					$hub = home_url().'/portal/hub-page/'.$user->data->user_nicename;
					$page_id = $this->select_page_id($hub);
					if(!$page_id){
						$post_hub_pgid = $this->create_hubpage($user->data->user_nicename);
						update_user_meta($user_id, 'wpc_cl_hubpage_id', $post_hub_pgid);
					}
					
					$portal = home_url().'/portal/portal-page/'.$user->data->user_nicename;
					$page_id = $this->select_page_id($portal);
					if(!$page_id){
						$post_clients_pgid = $this->create_clientspage($user->data->user_nicename);
						$this->assign_clients_portal_page($post_clients_pgid,$user_id);
					}
					
				}
			}
		}
		
		function select_page_id($guid){
			global $wpdb;
			
			$query = "SELECT ID FROM {$wpdb->prefix}posts where guid = '{$guid}'";
			return $wpdb->get_var($query);
		}
		
		function custom_aweber_form(){
			ob_start();
			
			$defatuls = $this->settings_default();
			$saved_data = get_option('_aspk_aweber_settings',$defatuls);
			$list_id    =  $saved_data['list_id'];?>
			<div id="mc4wp-form-1" class="form mc4wp-form">
				<form target="_blank" method="post" action="https://www.aweber.com/scripts/addlead.pl">
					<p>
						<label>Subscribe to our newsletter to receive money off coupons/vouchers, news and flash sale info</label>
						<input style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;float: left;height: 60px;width: 400px;padding: 15px 30px;border: none;background: #fff;margin-bottom: 0;text-transform: uppercase;color: #c8c8c8;font-size: 18px;line-height: 30px;font-weight: 400;" type="email" name="email" value="" placeholder="ENTER YOUR EMAIL..." required=""/>
					</p>
					<p>
						<input style="margin-left: 15px;padding: 18px 45px;font-size: 18px;line-height: 24px;" type="submit" name="submit" value="Subscribe" /> 
					</p>
					<input type="hidden" name="listname" value="awlist<?php echo $list_id;?>"/>
					<input type="hidden" name="redirect" value="" />
					<input type="hidden" name="meta_adtracking" value="custom form" />
					<input type="hidden" name="meta_message" value="1" /> 
					<input type="hidden" name="meta_required" value="email" /> 
					<input type="hidden" name="meta_forward_vars" value="1" /> 
				</form>
			</div>
			<?php
			return ob_get_clean();
		}
		
		
		function create_hubpage($user_name){
			$guid = home_url().'/portal/hub-page/'.$user_name;
			$post = array(
				'post_content'   =>  	'[wpc_client_hub_page_template /]' ,
				'post_name'      => 	$user_name , 
				'post_title'     => 	$user_name , 
				'post_status'    => 	'publish' , 
				'post_type'      =>  	'hubpage'  ,
				'post_author'    =>  	1 , 
				'guid'   		 =>  	$guid , 
				'post_date'      =>  	date('Y-m-d H:i:s') ,
				'post_date_gmt'  =>  	date('Y-m-d H:i:s')
			);  
			return wp_insert_post( $post);
		}
		
		function create_clientspage($user_name){
			ob_start();
			?>
			<p>[wpc_client]<span style="font-size: medium;">Welcome smith12 to your first Portal Page<span style="font-size: small;"> | [wpc_client_get_page_link page="hub" text="HUB Page"] | [wpc_client_logoutb]</span></span></p>
			<p>We'll be using this page to relay information and graphics to you.</p>
			<p>You can use the private messaging feature at the bottom of each page if you'd like to communicate with us, and all of our interaction will be here in one place.</p>
			<p>Thanks!</p>
			<table style="width: 100%;" border="0" cellspacing="0" cellpadding="0" align="center">
			<tbody>
			<tr>
			<td style="width: 50%; height: 70px;" valign="top"></td>
			<td style="width: 50%;" valign="top"></td>
			</tr>
			<tr>
			<td colspan="2" valign="top"><img title="" alt="" src="[wpc_client_theme][/wpc_client_theme]/messages.png" /></td>
			</tr>
			<tr>
			<td colspan="2" valign="top">[wpc_client_com][/wpc_client_com]</td>
			</tr>
			</tbody>
			</table>
			<p>[/wpc_client]</p>
			<?php
			$html = ob_get_clean();
			$guid = home_url().'/portal/portal-page/'.$user_name;
			$post = array(
				'post_content'   =>  	$html ,
				'post_name'      => 	$user_name ,
				'post_title'     => 	$user_name , 
				'post_status'    => 	'publish' , 
				'post_type'      =>  	'clientspage',
				'post_author'    =>  	1 , 
				'guid'   		 =>  	$guid , 
				'post_date'      =>  	date('Y-m-d H:i:s') , 
				'post_date_gmt'  =>  	date('Y-m-d H:i:s')
			);  
			return wp_insert_post( $post);
		}
		
		
		function __construct() {
			add_shortcode('AWEBER_FORM', array(&$this,'custom_aweber_form' ));
			add_action('admin_footer', array(&$this,'admin_footer'));
			add_action ('woocommerce_order_status_processing', array(&$this, 'aspk_operation'), 10 );
            add_action ('woocommerce_order_status_completed', array(&$this, 'aspk_operation'), 10 );
            add_action ('wp_footer', array(&$this, 'wp_footer'), 100 );
            add_action( 'wp_ajax_nopriv_chk_email', array(&$this,'chk_email' ));
            add_action( 'admin_menu', array(&$this, 'admin_menu') );
            add_action( 'wp_head', array(&$this, 'wp_head') );


		}
		
		function wp_head(){
			?>
				<style>
					#subscribe_to_aweber_field{ display: none;}
				</style>
			<?php
		}
		function admin_footer(){
			global $wpdb;
			
			$user_id = get_current_user_id();
			if($user_id == 13 || $user_id == 14){
				?>
				<script>
					jQuery('#toplevel_page_wpclients').remove();
				</script>
				<?php 
			}else{
				?>
				<script>
					jQuery('#toplevel_page_aspk_wcp_coach').remove();
				</script>
				<?php 
			}
			?>
			<script>
				jQuery( document ).ready(function() {
					jQuery('#subscribe_to_aweber_field').hide();
				});
			</script>
			<?php 
			
		}
		
		function admin_menu() {
			add_menu_page('WPC Support','WPC Support','read','aspk_wcp_coach',array( $this, 'wcp_coach' ));
			add_options_page( 'Aweber Settings', 'Aweber Settings', 'manage_options', 'aweber_settings', array(&$this, 'aweber_settings'));
		}
		
		function settings_default(){
			$defatuls = array();
			$defatuls['consumer_key'] = '';
			$defatuls['consumer_secret'] = '';
			$defatuls['account_id'] = '';
			$defatuls['list_id'] = '';
			return $defatuls;
		}
		
		function aweber_settings(){
			$settings = array();
			if(isset($_POST['save_aweber_settings'])){
				$settings['consumer_key'] = $_POST['consumer_key'];
				$settings['consumer_secret'] = $_POST['consumer_secret'];
				$settings['account_id'] = $_POST['account_id'];
				$settings['list_id'] = $_POST['list_id'];
				update_option('_aspk_aweber_settings',$settings);
			}
			$defatuls = $this->settings_default();
			$saved_data = get_option('_aspk_aweber_settings',$defatuls);
			
			?>
				<div style="float:left;clear:left;padding:2em;background-color:#FFFFFF;margin-top: 1em;"> 
					<?php
					if(isset($_POST['save_aweber_settings'])){
						?>
						<div id="rets_save_message" class="updated" style="float:left;clear:left;">
							Settings have been saved
						</div>
						<?php
					}
					?>
					<div style="float:left;clear:left;">
						<h3> Aweber Settings </h3>
					</div>
					<div style="float:left;clear:left;">
						<form action="" method="post">
							<div style="float:left;clear:left;margin-top:1em;">
								<div style="clear:left;">
									<div style="float:left;width: 9em;">Consumer Key</div>
									<div style="margin-left:1em;float:left;">
										<input required style="width:20em;" type="text" name="consumer_key" value="<?php if(isset($saved_data['consumer_key'])) echo $saved_data['consumer_key'];  ?>"/>
									</div>
								</div>
								<div style="clear:left;">
									<div style="float:left;width: 9em;">Consumer Secret</div>
									<div style="margin-left:1em;float:left;">
										<input required style="width:20em;" type="text" name="consumer_secret" value="<?php if(isset($saved_data['consumer_secret'])) echo $saved_data['consumer_secret'];  ?>"/>
									</div>
								</div>
								<div style="clear:left;">
									<div style="float:left;width: 9em;">Account Id</div>
									<div style="margin-left:1em;float:left;">
										<input required style="width:20em;" type="text" name="account_id" value="<?php if(isset($saved_data['account_id'])) echo $saved_data['account_id'];  ?>"/>
									</div>
								</div>
								<div style="clear:left;">
									<div style="float:left;width: 9em;">List Id</div>
									<div style="margin-left:1em;float:left;">
										<input required style="width:20em;" type="text" name="list_id" value="<?php if(isset($saved_data['list_id'])) echo $saved_data['list_id'];  ?>"/>
									</div>
								</div>
								
								<div style="float:left;clear:left;margin-top:1em;">
									<div style="float:left;">
										<input class="button button-primary" type="submit" name="save_aweber_settings" value="Save"/>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<script>
					setTimeout(function(){ 
						jQuery('#rets_save_message').hide();
					}, 8000);
				</script>
			<?php
		}
		
		function wp_clients_invoices(){
			 $args = array(
				'orderby'          => 'date',
				'order'            => 'DESC',
				'post_type'        => 'shop_order',
				'post_status'      => array('wc-processing' , 'wc-completed')
			);
			return get_posts( $args ); 
			
		}
		
		function wcp_coach(){
			$users = $this->wcp_user();
			$url = home_url();
			?>
			<div style = "float:left;clear:left;padding:1em;background:#FFFFFF;">
				<?php
				if(!empty($users)){ ?>
					<div style = "float:left;clear:left;"><h2>WPC Support</h2></div>
					<div style = "float:left;clear:left;margin-top:1em;">
						<div style = "float:left;width:25em;"><b>User Name</b></div>
						<div style = "float:left;width:20em;"><b>Client Hub Page</b></div>
						<div style = "float:left;width:20em;"><b>Client Portal Page</b></div>
					</div>
				<?php
					foreach($users as $user){
						?>
						<div style = "float:left;clear:left;margin-top:1em;">
							<div style = "float:left;width:25em;"><?php echo $user->data->user_nicename; ?></div>
							<div style = "float:left;width:20em;"><a href = "<?php echo $url.'/portal/hub-page/'.$user->data->user_nicename;?>" >Hub Page</a></div>
							<div style = "float:left;width:20em;"><a href = "<?php echo $url.'/portal/portal-page/'.$user->data->user_nicename;?>" >Portal Page</a></div>
						</div>
						<?php
					}
				}else{
					?><div style = "float:left;clear:left;"><h2>No User Found</h2></div><?php
				}
			?>
			</div>
			<?php
		}
		
		function wcp_user(){
			$args = array(
				'blog_id'      => $GLOBALS['blog_id'],
				'role'         => 'wpc_client'
			 );
			return get_users( $args );
		}
		
		function chk_email(){
			$email = $_POST['email'];
			$user = get_user_by( 'email', $email );
			if(!empty($user)){
				echo 'yes';
			}else{
				echo 'no';
			}
			exit;
		}
		
		function wp_footer(){
			global $post,$wpdb; 
			
			$current_user = wp_get_current_user();
			if(isset($current_user->data->ID)){
				$user_role = get_user_meta($current_user->data->ID,$wpdb->prefix.'capabilities', true);
				if($user_role){
					$role = array_keys($user_role);
					if($role[0] == 'wpc_client'){
						$hub_page_url = home_url().'/portal/hub-page/'.$current_user->data->user_nicename;
						$portal_page_url = home_url().'/portal/portal-page/'.$current_user->data->user_nicename;?>
						<script>
							jQuery( document ).ready(function() {
								jQuery( "#topnav a:contains('Client Area')").attr('href','<?php echo $hub_page_url; ?>');
								jQuery( "#topnav a:contains('Client Communication')").attr('href','<?php echo $portal_page_url; ?>');
							});
						</script>
					<?php
					}
				}
			}
			
			if(strstr(get_permalink(),"hub-page")){
				$user_id = get_current_user_id();
				$invoices_order = $this->wp_clients_invoices();
				$url = home_url('checkout/order-received/');
				if(!empty($invoices_order)){
					?><div id = "invoice_order"><?php
					foreach($invoices_order as $inv_ord){
						$meta_values 	= get_post_meta( $inv_ord->ID );
						$order_key 	= get_post_meta( $inv_ord->ID,'_order_key', true);
						$order_user_id 		= $meta_values['_customer_user']['0'];
						if($user_id == $order_user_id){
							?>
							<div style = "float:left;clear:left;">
								<div style="float:left;"><?php echo $inv_ord->ID;?></div>
								<div style="float:left;margin-left:7em;"><a href="<?php echo $url.$inv_ord->ID.'/?key='.$order_key.'&pdfid='.$inv_ord->ID;?>">Invoice</a></div>
							</div>
							
							<?php
						}
					}
					?></div>
						<script>
							jQuery( document ).ready(function() {
								var x = jQuery('#invoice_order').html();
								jQuery('.button_addfile').append(x);
								jQuery('#invoice_order').html('');
							});
						</script>
					<?php
				}
			} 
			
			if($post->post_title == 'Checkout' && !is_user_logged_in()){
				?>
					<script>
						jQuery( document ).ready(function() {
							jQuery('#createaccount').attr('checked','checked');
							jQuery('#createaccount').css('pointer-events','none');
							jQuery('.create-account').css('display','block');
							jQuery("#billing_email").focusout(function(){
								var data = {
									'action'   : 'chk_email',
									'email' : jQuery('#billing_email').val(),
						
								};
								var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' );?>';
									// We can also pass the url value separately from ajaxurl for front end AJAX implementations
									jQuery.post(ajaxurl, data, function(response) {
										if(response == 'yes'){
											jQuery('#place_order').attr('disabled','disabled');
											alert('User already Registered with this email, please use an other email');
										}else if(response == 'no'){
											jQuery('#place_order').removeAttr('disabled');
										}
								});
							});
						});
					</script>
				<?php
			}
		}
		
		function aweber_add_subscriber($user_email){
			/* $defatuls = $this->settings_default();
			$saved_data = get_option('_aspk_aweber_settings',$defatuls);
			
			$consumerKey    =  $saved_data['consumer_key']; 
			$consumerSecret =  $saved_data['consumer_secret'];
			$account_id     =  $saved_data['account_id'];
			$list_id        =  $saved_data['list_id'];
			
			$aweber = new AWeberAPI($consumerKey, $consumerSecret);
			$aweber->user->requestToken = 'Aq56KTbBINorxxcaHW3RVmUa';
			$aweber->user->verifier = '7qdtrq';
			//$aweber->user->tokenSecret = $_COOKIE['secret'];
			//list($accessTokenKey, $accessTokenSecret) = $aweber->getAccessToken();
			$accessKey = 'Agmzrox4qXGaAIlgyp29M5sp';
			$accessSecret = '8XWhCEpt87n8USotCPsnaCdsplIGkeLopAiDok64';
			try {
				$account = $aweber->getAccount($accessKey, $accessSecret);
				$listURL = "/accounts/{$account_id}/lists/{$list_id}/subscribers/52636832";
				$list = $account->loadFromUrl($listURL);
				if($account->lists){
					foreach ($account->lists as $list) {
						$params = array(
						'email' => $user_email
						);
						$subscribers = $list->subscribers;
						$new_subscriber = $subscribers->create($params);
					}
				}
			}catch(AWeberAPIException $exc) {
			} */
		} 
		
		function assign_clients_portal_page($pid,$uid){
			global $wpdb;
			
			$sql = "insert into {$wpdb->prefix}wpc_client_objects_assigns (object_type,object_id,assign_type,assign_id) values('portal_page','{$pid}','client','{$uid}')";
			$wpdb->query($sql);
		}
		
	}//class ends
}//existing class ends
$aspk_wp_client = new aspk_wp_client();